﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Banco.Interface
{
    public interface IConversor
    {
        [Key]
        int Id { get;}
        [Required]
        string Tipo { get;}
        float CotacaoDiaria { get;}
        bool IsCompleted { get; }

        float ConverterMoeda(float quantia);
    }
}
